package vip.guzb.sample.test;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * SpeakNameTask
 *
 * @author 顾志兵
 * @mail ipiger@163.com
 * @since 2024-04-09
 */
public class SpeakNameTask implements Runnable{
    private String name;

    public SpeakNameTask(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        Random random = new Random();
        int milliseconds = 500 + random.nextInt(1000);
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
            System.out.println("["+Thread.currentThread().getName()+"]: I believe " + name);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
