package vip.guzb.sample.test;

import vip.guzb.sample.threadpool.step01.SingleThreadPoolExecutor3;

import java.util.concurrent.TimeUnit;

/**
 * @author 顾志兵
 * @mail ipiger@163.com
 * @since 2024-04-08
 */
public class SingleThreadPoolExecutor3Test {

    public static void main(String[] args) throws InterruptedException {
        SingleThreadPoolExecutor3 stp = new SingleThreadPoolExecutor3();
        for (int i = 1; i <= 20; i++) {
            stp.execute(new SpeakNameTask("Coding Change The World " + i));
        }
        TimeUnit.SECONDS.sleep(10);
        stp.shutdown();
    }

}
