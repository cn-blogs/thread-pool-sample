package vip.guzb.sample.test;

import vip.guzb.sample.threadpool.step02.MultiThreadPoolExecutor;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author 顾志兵
 * @mail ipiger@163.com
 * @since 2024-04-09
 */
public class MultiThreadPoolExecutorTest {

    final static Random random = new Random();
    final static MultiThreadPoolExecutor executor = new MultiThreadPoolExecutor();

    public static void main(String[] args) throws InterruptedException {
        for (int i = 1; i <= 20; i++) {
            addTask("Coding Change The World " + i);
        }
        System.out.println("-----------------");
        executor.printAllRunners();

        TimeUnit.SECONDS.sleep(3);
        System.out.println("-----------------");
        executor.printAllRunners();

        TimeUnit.SECONDS.sleep(2);
        System.out.println("-----------------");
        executor.printAllRunners();

        TimeUnit.SECONDS.sleep(3);
        System.out.println("-----------------");
        executor.printAllRunners();
    }

    private static void addTask(String name) throws InterruptedException {
        executor.execute(new SpeakNameTask(name));
        int milliseconds = 200 + random.nextInt(300);
        TimeUnit.MILLISECONDS.sleep(milliseconds);
    }

}
