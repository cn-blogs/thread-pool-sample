package vip.guzb.sample.test;

import vip.guzb.sample.threadpool.step01.SingleThreadPoolExecutor1;

/**
 * @author 顾志兵
 * @mail ipiger@163.com
 * @since 2024-04-08
 */
public class SingleThreadPoolExecutor1Test {

    public static void main(String[] args) {
        SingleThreadPoolExecutor1 stp = new SingleThreadPoolExecutor1();
        for (int i = 1; i <= 10; i++) {
            stp.execute(new SpeakNameTask("Coding Change The World " + i));
        }
        System.out.println("主线程已结束");
    }

}
