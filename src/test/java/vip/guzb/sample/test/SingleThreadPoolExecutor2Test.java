package vip.guzb.sample.test;

import vip.guzb.sample.threadpool.step01.SingleThreadPoolExecutor2;

import java.util.concurrent.TimeUnit;

/**
 * @author 顾志兵
 * @mail ipiger@163.com
 * @since 2024-04-08
 */
public class SingleThreadPoolExecutor2Test {

    public static void main(String[] args) throws InterruptedException {
        SingleThreadPoolExecutor2 stp = new SingleThreadPoolExecutor2();
        for (int i = 1; i <= 5; i++) {
            stp.execute(new SpeakNameTask("Coding Change The World " + i));
        }
        TimeUnit.SECONDS.sleep(10);
        for (int i = 6; i <= 10; i++) {
            stp.execute(new SpeakNameTask("Coding Change The World " + i));
        }
        TimeUnit.SECONDS.sleep(10);
        for (int i = 11; i <= 15; i++) {
            stp.execute(new SpeakNameTask("Coding Change The World " + i));
        }
    }

}
